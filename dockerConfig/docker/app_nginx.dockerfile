FROM php:7.3-fpm

RUN apt-get update -y \
    && apt-get install -y nginx

# PHP_CPPFLAGS are used by the docker-php-ext-* scripts
ENV PHP_CPPFLAGS="$PHP_CPPFLAGS -std=c++11"

COPY ../http/nginx-site.conf /etc/nginx/sites-enabled/default
COPY ../http/entrypoint.sh /etc/entrypoint.sh

COPY --chown=www-data:www-data . /var/www/html
RUN chmod 777 /etc/entrypoint.sh
WORKDIR /var/www/html

EXPOSE 80 443

#ENTRYPOINT ["/etc/entrypoint.sh"]




