require('./bootstrap');
// Require Vue
window.Vue = require('vue').default;

// Register Vue Components
Vue.component('league', require('../components/league.vue').default);

// Initialize Vue
const app = new Vue({
    el: '#app',
});
