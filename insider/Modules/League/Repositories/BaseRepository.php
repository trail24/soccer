<?php

namespace Modules\League\Repositories;

use Exception;
use Illuminate\Container\Container as Application;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Symfony\Component\Translation\Exception\NotFoundResourceException;


/**
 * Class BaseRepository
 * @package Modules\Core\Repositories;
 */
abstract class BaseRepository
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * @var Application
     */
    protected $app;


    public function __construct(Application $app,$model = null)
    {
        $this->app = $app;
        $this->model = $model ?? $this->makeModel();
    }

    /**
     * Make Model instance
     *
     * @return Model
     * @throws Exception
     *
     */
    public function makeModel()
    {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model) {
            throw new Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }

        return $this->model = $model;
    }

    /**
     * Configure the Model
     *
     */
    abstract public function model();


    /**
     * Build a query for retrieving all records.
     *
     * @param string   $search
     * @param int|null $skip
     * @param int|null $limit
     * @param array    $relations
     * @return Builder
     */
    public function allQuery($search = '', array $relations = [], $skip = null, $limit = null)
    {
        $query = $this->model->newQuery()->with($relations);

        if ($search) {
            $searchable = $this->getFieldsSearchable();
            $query->where(
                function ($q) use ($searchable, $search) {
                    foreach ($searchable as $field) {
                        $q->orWhere($field, 'like', "%{$search}%");
                    }
                }
            );
        }

        if (!is_null($skip)) {
            $query->skip($skip);
        }

        if (!is_null($limit)) {
            $query->limit($limit);
        }

        return $query;
    }

    /**
     * Get searchable fields array
     *
     * @return array
     */
    abstract public function getFieldsSearchable(): array;

    /**
     * Retrieve all records with given filter criteria
     *
     * @param array    $search
     * @param array    $relations
     * @param int|null $skip
     * @param int|null $limit
     * @param array    $columns
     *
     * @return LengthAwarePaginator|Builder[]|Collection
     */
    public function all($search = '', array $relations = [], $skip = null, $limit = null, $columns = ['*'])
    {
        $query = $this->allQuery($search, $relations, $skip, $limit);
        return $query->get($columns);
    }


    /**
     * Create model record
     *
     * @param array $input
     *
     * @return Model
     */
    public function create(array $input): Model
    {
        $model = $this->model->newInstance($input);

        $model->save();

        return $model;
    }


    /**
     * Update model record for given id
     *
     * @param array $input
     * @param int   $id
     *
     * @return Builder|Builder[]|Collection|Model
     */
    public function update(array $input, int $id)
    {
        $this->checkId($id);

        $query = $this->model->newQuery();

        $model = $query->findOrFail($id);

        $model->fill($input);

        $model->save();

        return $model;
    }

    /**
     * check input id
     * @param $id
     * @return JsonResponse/Void
     */
    public function checkId($id)
    {
        if (!is_int($id)) {
            throw new NotFoundResourceException('Not Valid ID',404);
        }
    }

    /**
     * duplicate model
     * @param $id
     * @return Model
     */
    public function duplicate($id): Model
    {
        $model = $this->find($id);
        return $model->replicate();
    }

    /**
     * Find model record for given id
     *
     * @param int   $id
     * @param array $load : relations
     * @param array $columns
     * @return Builder|Builder[]|Collection|Model|null
     */
    public function find(int $id, $load = [], $columns = ['*'])
    {
        $query = $this->model->newQuery();

        return $query->find($id, $columns);
    }


    /**
     * Get model record on column
     * @param $column
     * @param $value
     * @param array $relations
     * @return Builder
     */
    public function where($column,$value = null,$relations = [])
    {
        $query = $this->model->newQuery();

        return $query->where($column, $value)->with($relations);
    }

    /**
     * @param int $id
     *
     * @return bool|mixed|null
     * @throws Exception
     *
     */
    public function delete($id): ?bool
    {
        $this->checkId($id);

        $query = $this->model->newQuery();

        $model = $query->findOrFail($id);

        return $model->delete();
    }




}
