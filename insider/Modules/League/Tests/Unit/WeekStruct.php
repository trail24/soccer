<?php

namespace Modules\League\Tests\Unit;


class WeekStruct
{


    public static function success()
    {
        return [
            'data' => [
                'cols' =>
                    [
                        '*' => [
                            'team',
                            'point',
                            'w',
                            'd',
                            'l',
                            'gd',
                            'estimate',
                        ]
                    ]
            ]
        ];
    }

    public static function fail()
    {
        return [
            'meta' => [
                'status'
            ]
        ];
    }
}
