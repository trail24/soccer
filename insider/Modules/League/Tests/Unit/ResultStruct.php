<?php

namespace Modules\League\Tests\Unit;


class ResultStruct
{


    public static function success()
    {
        return [
            'data' => [
                '*' => [
                    'first_team' => ['name'],
                    'first_goals',
                    'second_team' => ['name'],
                    'second_goals',
                ]
            ]
        ];
    }
}
