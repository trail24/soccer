<?php

namespace Modules\League\Tests\Unit;

use Tests\TestCase;

class WeeksTest extends TestCase
{


    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testWeekSuccess()
    {
        $response = $this->post('api/week',['id'=>1]);
        $response->assertJsonStructure(
            WeekStruct::success()
        )
            ->assertStatus(200);
    }


    public function testWeekFail()
    {
        $response = $this->post('api/week',['id'=>'a']);
        $response->assertJsonStructure(
            WeekStruct::fail()
        )
            ->assertStatus(500);
    }


}
