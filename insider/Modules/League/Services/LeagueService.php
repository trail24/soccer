<?php


namespace Modules\League\Services;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\League\Http\Requests\GetRequest;
use Modules\Match\Models\Matcher;
use Modules\Week\Models\Week;
use Modules\Week\Services\WeekService;

class LeagueService
{


    /**
     * @var \Illuminate\Contracts\Foundation\Application|mixed
     */
    private $weekService;

    public function __construct()
    {
        $this->weekService = resolve(WeekService::class);

    }

    /**
     * @param GetRequest $request
     * @return array
     */
    public function get(GetRequest $request)
    {
        //
        $input = $request->validated();

        $result['cols'] = $this->weekService->init($input['id']);
        $result['week'] =$this->weekService->week->id;

        return $result;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function play(Request $request)
    {
        $count = 0;
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Week::truncate();

        while ($count < 6) {
            $count++;
            $result = $this->weekService->init($count);
        }
        $response['teams'] = $result;
        $response['week'] = $count;
        return $response;
    }

    /**
     * @return array
     */
    public function reset()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Matcher::truncate();
        Week::truncate();

        $result = $this->weekService->init(1);

        $response['teams'] = $result;
        $response['week'] = 1;
        return $response;
    }





}
