<?php


namespace Modules\League\Http\Controllers;


use Illuminate\Routing\Controller;
use Modules\League\Exceptions\ExceptionHandler;
use Modules\League\Traits\ApiResponse;

class BaseController extends Controller
{
    use ExceptionHandler;
}
