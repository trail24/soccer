<?php

namespace Modules\League\Http\Controllers;

use Illuminate\Http\Request;
use Modules\League\Http\Requests\GetRequest;
use Modules\League\Services\LeagueService;

class LeagueController extends BaseController
{
    protected $service;

    public function __construct()
    {
        $this->service = resolve(LeagueService::class);
    }

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(GetRequest $request)
    {
        try {
            return $this->setMetaData($this->service->get($request))
                ->successResponse();
        } catch (\Exception $exception) {
            return $this->handleException($request, $exception);
        }
    }

    public function play(Request $request)
    {
        try {
            return $this->setMetaData($this->service->play($request))
                ->successResponse();
        } catch (\Exception $exception) {
            return $this->handleException($request, $exception);
        }
    }

    public function reset(Request $request)
    {
        try {
            return $this->setMetaData($this->service->reset($request))
                ->successResponse();
        } catch (\Exception $exception) {
            return $this->handleException($request, $exception);
        }
    }


}
