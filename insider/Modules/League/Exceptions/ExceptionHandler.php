<?php


namespace Modules\League\Exceptions;


use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Modules\League\Traits\ApiResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

trait ExceptionHandler
{
    use ApiResponse;


    private function handleApiException($request, \Throwable $e)
    {
        return $this->handleException($request, $e);
    }


    //status default is Failed

    public function handleException($request, $e, $httpCode = null, $status = 'Failed')
    {
        if ($e instanceof NotFoundHttpException) {
            return $this->handle($e, $e->getMessage(), $this->getHttpCode(404, $httpCode), $e->getCode(), $status);
        } elseif ($e instanceof NotFoundResourceException) {
            return $this->handle($e, $e->getMessage(), $this->getHttpCode(404, $httpCode), $e->getCode(), $status);
        } elseif ($e instanceof ModelNotFoundException) {
            return $this->handle($e, $e->getMessage(), $this->getHttpCode(404, $httpCode), $e->getCode(), $status,$e->getModel() .' ID: '. implode(',',$e->getIds()));
        } elseif ($e instanceof ValidationException) {
            $e = $this->convertValidationExceptionToResponse($e, $request); return $this->setMetaData([], $e->getOriginalContent())->badRequestResponse(100);
        }  elseif ($e instanceof \Lauthz\Exceptions\UnauthorizedException) {
            return $this->handle($e, $e->getMessage(), $this->getHttpCode(400, $httpCode), $e->getCode(), $status);
        } else {
//            return $e;
            return $this->customResponse($e, $status, 500, 0);
            return $this->failedResponse();
        }
    }

    /**
     * @param \Exception $e
     * @param        $message
     * @param        $httpCode
     * @param        $statusCode
     * @param string $status
     * @param $additional
     * @return \Illuminate\Http\JsonResponse
     */
    private function handle(\Exception $e, $message, $httpCode, $statusCode, $status,$additional = null)
    {
        Log::error($e);
        return $this->customResponse($message. ' '.$additional, $status, $httpCode, $statusCode);
    }

    private function getHttpCode($defaultHttpCode, $currentHttpCode)
    {
        $httpCode = $defaultHttpCode;
        if (isset($currentHttpCode) && $currentHttpCode != null) {
            $httpCode = $currentHttpCode;
        }
        return $httpCode;
    }
}
