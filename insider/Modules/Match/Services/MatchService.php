<?php


namespace Modules\Match\Services;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Match\Http\Requests\UpdateRequest;
use Modules\Match\Repositories\MatchRepository;
use Modules\Team\Repositories\TeamRepository;
use Modules\Week\Models\Week;
use Modules\Week\Services\WeekService;

class MatchService
{
    /**
     * @var Week
     */
    private $week;
    /**
     * @var \Illuminate\Contracts\Foundation\Application|mixed
     */
    private $reposiotry;
    /**
     * @var \Illuminate\Contracts\Foundation\Application|mixed
     */
    private $teamReposiotry;

    public function __construct(Week $week)
    {
        $this->week = $week;
        $this->reposiotry = resolve(MatchRepository::class);
        $this->teamReposiotry = resolve(TeamRepository::class);
    }

    /**
     * Create matches for week
     */
    public function boot()
    {
//        $result = Week::whereBetween('id', [0, $this->week->id])->get()->
//        map(
//            function ($week) {
//                $this->week = $week;
//            }
//        );

        if($this->week->matches()->get()->isNotEmpty())
        return ;

        if($this->week->id <= 3)
            $this->NotPlayedBefore();
        else
            $this->reposiotry->where('week_id',$this->week->id - 3)->get()->map(function ($match){
               $this->foundTeam($match->secondTeam()->first(),$match->firstTeam()->first());
           });

    }


    public function get(Request $request)
    {
        $weekService = resolve(WeekService::class);

        $this->week = $weekService->get($request->id);

        return $this->week->matches()
            ->with(['firstTeam','secondTeam'])
            ->get();
    }


    public function NotPlayedBefore()
    {
        return $this->teamReposiotry->all()->random(2)
            ->map(
                function ($firstTeam){
                    $this->findSecTeam($firstTeam);
                }
            );
    }

    public function findSecTeam($firstTeam,$count=1)
    {
        $result = $this->teamReposiotry->all()
            ->filter(
                function ($secTeam) use ($firstTeam) {
                    if ($firstTeam->id == $secTeam->id) {
                        return false;
                    }
                    DB::enableQueryLog();
                    $found = $this->reposiotry->where(function($query)use($firstTeam,$secTeam) {
                                    $query
                                        ->where('week_id','<=',3)
                                        ->where(function ($query)use ($firstTeam,$secTeam){
                                            $query->where('first_team',$firstTeam->id)
                                                ->where('second_team',$secTeam->id);
                                        })
                                        ->orWhere(function ($query)use ($firstTeam,$secTeam){
                                            $query->where('first_team',$secTeam->id)
                                            ->where('second_team',$firstTeam->id);
                                        });
                    })->get();




                    $found2 = $this->week->matches()
                        ->where(function($query)use($firstTeam,$secTeam){
                            $query->whereIn('first_team', [$secTeam->id,$firstTeam->id])
                                ->orWhereIn('second_team', [$secTeam->id,$firstTeam->id]);
                        })
                        ->get();

//dd(DB::getQueryLog());
                    if ($found->isNotEmpty() || $found2->isNotEmpty() ) {
                        return false;
                    }

                    return $secTeam;
                });

        if($result->isEmpty() && $count<4) {
            $count++;
            $firstTeam = $this->teamReposiotry->find($count);
            return $this->findSecTeam($firstTeam, $count);
        }


        if(!empty($result->first()))
            $this->foundTeam($firstTeam,$result->first());
    }


    public function foundTeam( $first,$second )
    {
        $rnd =random_int(0,7);
        $match = [
            'first_team' => $first->id,
            'first_goals' => $this->strength($first,$rnd),
            'second_team' => $second->id ?? '',
            'second_goals' =>  $this->strength($second,$rnd),
        ];

        $this->week->matches()->create($match);
    }


    public function strength($team,$rnd)
    {
        $strength= $team->strength / 10;
        $result = $rnd*$strength;
        return (int) round($result);
    }


    /**
     * @param UpdateRequest $request
     */
    public function update(UpdateRequest $request)
    {

        $input = $request->validated();
        $this->reposiotry->update($input,$input['id']);
    }

}
