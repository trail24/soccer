<?php


namespace Modules\Match\Repositories;


use Modules\League\Repositories\BaseRepository;
use Modules\Match\Models\Matcher;

class MatchRepository extends BaseRepository
{

    public function model()
    {
        return Matcher::class;
    }

    public function getFieldsSearchable(): array
    {
        // TODO: Implement getFieldsSearchable() method.
    }
}
