<?php

namespace Modules\Match\Http\Controllers;

use Illuminate\Http\Request;
use Modules\League\Http\Controllers\BaseController;
use Modules\Match\Http\Requests\UpdateRequest;
use Modules\Match\Services\MatchService;

class MatchController extends BaseController
{

    /**
     * @param Request $request
     * @param MatchService $service
     * @return mixed
     */
    public function get(Request $request,MatchService $service)
    {
        try {
            return $this->setMetaData($service->get($request))
                ->successResponse();
        } catch (\Exception $exception) {
            return $this->handleException($request, $exception);
        }
    }

    public function update(UpdateRequest $request,MatchService $service)
    {
        try {
            return $this->setMetaData($service->update($request))
                ->successResponse();
        } catch (\Exception $exception) {
            return $this->handleException($request, $exception);
        }
    }
}
