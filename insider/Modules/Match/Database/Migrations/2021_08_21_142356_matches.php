<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Matches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches',function (Blueprint $table){
           $table->id();
            $table->foreignId('week_id')->nullable()->constrained()->cascadeOnDelete();
            $table->integer('first_team');
           $table->integer('first_goals');
           $table->integer('second_team');
           $table->integer('second_goals');
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
