<?php


namespace Modules\Match\Models;


use Illuminate\Database\Eloquent\Model;
use Modules\Team\Models\Team;

class Matcher extends Model
{

    protected $table='matches';

    protected $fillable = ['first_team','first_goals','second_team','second_goals'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function firstTeam()
    {
        return $this->hasOne(Team::class,'id','first_team');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function secondTeam()
    {
        return $this->hasOne(Team::class,'id','second_team');
    }

}
