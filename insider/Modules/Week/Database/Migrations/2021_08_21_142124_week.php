<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Week extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weeks',function (Blueprint $table){
            $table->id();
            $table->integer('pts')->nullable();
            $table->integer('pw')->nullable();
            $table->integer('pd')->nullable();
            $table->integer('pl')->nullable();
            $table->integer('gd')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
