<?php


namespace Modules\Week\Repositories;

use Modules\League\Repositories\BaseRepository;
use Modules\Week\Models\Week;

/**
 * Class WeekRepository
 */
class WeekRepository extends BaseRepository
{


    /**
     * Configure the Model
     */
    public function model(): string
    {
        return Week::class;
    }


    public function getFieldsSearchable(): array
    {
        // TODO: Implement getFieldsSearchable() method.
    }
}
