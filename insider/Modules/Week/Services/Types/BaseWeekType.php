<?php


namespace Modules\Week\Services\Types;


use Modules\Team\Models\Team;
use Modules\Week\Models\Week;

class BaseWeekType
{

    protected $week;
    /**
     * @var Team
     */
    protected $team;
    /**
     * @var Team
     */
    protected $teamRepository;

    public function __construct(Team $team, Week $week)
    {
        $this->team = $team;
        $this->week = $week;
        $this->teamRepository = new Team();

    }

    /** Get all weeks from creation
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function weeks()
    {
       return $this->week->newQuery()
           ->where('id','<=',$this->week->id)
           ->get();
    }

}
