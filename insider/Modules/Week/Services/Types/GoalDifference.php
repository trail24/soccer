<?php


namespace Modules\Week\Services\Types;


class GoalDifference extends BaseWeekType implements WeekType
{

    /** Get difference goals from goals and loseGoals
     * @return int
     */
    public function value(): int
    {
        return $this->goals() - $this->loseGoals();
    }


    /**
     * @return mixed
     */
    public function goals()
    {
        return $this->weeks()->reduce(
                function ($sum, $week) {
                    $sum += $week->matches()
                        ->where('first_team', $this->team->id)
                        ->sum('first_goals');
                    $sum += $week->matches()
                        ->where('second_team', $this->team->id)
                        ->sum('second_goals');
                    return $sum;
                });
    }

    /**
     * @return mixed
     */
    public function loseGoals()
    {
        return $this->weeks()->reduce(
            function ($sum, $week) {
                $sum += $week->matches()
                    ->where('first_team', $this->team->id)
                    ->sum('second_goals');
                $sum += $week->matches()
                    ->where('second_team', $this->team->id)
                    ->sum('first_goals');
                return $sum;
            });
    }


    public function estimate(): string
    {
        // TODO: Implement estimate() method.
    }


    public function point(): int
    {
        // TODO: Implement point() method.
    }
}
