<?php


namespace Modules\Week\Services\Types;


interface WeekType
{
    /**
     * @return Int
     */
    public function value():Int;

    /**
     * @return Int
     */
    public function point():Int;

    /**
     * @return string
     */
    public function estimate():string;
}
