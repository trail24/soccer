<?php


namespace Modules\Week\Services\Types;



use Modules\Team\Models\Team;
use Modules\Week\Models\Week;

class Point implements WeekType
{

    /**
     * @var array
     */
    private $types;
    /**
     * @var null
     */
    private $value;
    /**
     * @var Team
     */
    private $teamRepository;
    /**
     * @var Week
     */
    private $week;

    /** Sum all week types
     * Point constructor.
     * @param array $type
     * @param Week $week
     */
    public function __construct(array $type,Week $week)
    {
        $this->week = $week;
        $this->teamRepository = new Team();
        $this->types = $type;
        $this->value = null;
    }

    /** Sum point for each team
     * @return int
     */
    public function value(): int
    {
        foreach ($this->types as $type)
            $this->value += $type->point();

        return $this->value;
    }

    /** get all points
     * @return mixed
     */
    public function all()
    {
        return $this->teamRepository->all()->reduce(function ($sum ,$team){
            $win = new Win($team,$this->week);
            $draw = new Draw($team,$this->week);

            $sum += $win->point();
            $sum += $draw->point();
            return $sum;
        });

    }

    public function point(): int
    {
        // TODO: Implement point() method.
    }

    public function estimate(): string
    {
        return (int)($this->value / $this->all() * 100);
    }
}
