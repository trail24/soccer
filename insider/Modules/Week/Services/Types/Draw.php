<?php


namespace Modules\Week\Services\Types;



class Draw extends BaseWeekType implements WeekType
{

    /** Count draw games
     * @return int
     */
    public function value(): int
    {
        return $this->weeks()->map(function ($week){
            return $week->matches()
                ->where(function ($query) {
                    $query->where(function ($query) {
                    $query->where('first_team', $this->team->id)
                        ->whereRaw('first_goals = second_goals');
                })
                ->orWhere(function ($query) {
                    $query->where('second_team', $this->team->id)
                        ->whereRaw('second_goals = first_goals');
                });})->count();
        })->sum();
    }


    public function estimate(): string
    {
        // TODO: Implement estimate() method.
    }

    /**
     * @return int
     */
    public function point(): int
    {
        return $this->value() ;
    }
}
