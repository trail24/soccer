<?php


namespace Modules\Week\Services;


use Modules\Match\Services\MatchService;
use Modules\Team\Models\Team;
use Modules\Team\Repositories\TeamRepository;
use Modules\Week\Models\Week;
use Modules\Week\Repositories\WeekRepository;
use Modules\Week\Services\Types\Draw;
use Modules\Week\Services\Types\GoalDifference;
use Modules\Week\Services\Types\Lose;
use Modules\Week\Services\Types\Point;
use Modules\Week\Services\Types\Win;

class WeekService
{
    /**
     * @var WeekRepository|null
     */
    protected $repository;
    /**
     * @var Week
     */
    public $week;
    /**
     * @var \Illuminate\Contracts\Foundation\Application|mixed
     */
    private $teamReposiotry;

    /**
     * WeekService constructor.
     * @param WeekRepository|null $repository
     */
    public function __construct(WeekRepository $repository)
    {
        $this->repository = $repository;
        $this->teamReposiotry = resolve(TeamRepository::class);

    }

    /**
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|Team[]
     */
    public function init(int $id)
    {
        $this->get($id);

        $match = new MatchService($this->week);
        $match->boot();

        return $this->make();
    }


    public function get($id)
    {
        if($id>6) $id=6;
        $this->week = $this->repository->find($id);

        if (!$this->week) {
            $this->week = $this->repository->create(['id' => $id]);
        }

        return $this->week;
    }

    public function make()
    {
        $result = $this->teamReposiotry->all()->map(function ($team){
             return $this->result($team);
        });

        return $result;
    }

    /** Output result
     * @param $team
     * @return array
     */
    public function result($team)
    {
        $W = new Win($team, $this->week);
        $D = new Draw($team, $this->week);
        $L = new Lose($team, $this->week);
        $GD = new GoalDifference($team, $this->week);
        $point = new Point([$W,$D],$this->week);

        return [
            'team' => $team->name,
            'point' => $point->value(),
            'w' => $W->value(),
            'd' => $D->value(),
            'l' => $L->value(),
            'gd' =>$GD->value(),
            'estimate' => $this->week->id > 3 ? $point->estimate() : ''
        ];
    }


}
