<?php


namespace Modules\Week\Models;


use Illuminate\Database\Eloquent\Model;
use Modules\Match\Models\Matcher;

class Week extends Model
{

    protected $fillable = ['id'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function matches()
    {
        return $this->hasMany(Matcher::class);
    }
}
