<?php


namespace Modules\Team\Repositories;


use Modules\League\Repositories\BaseRepository;
use Modules\Team\Models\Team;

class TeamRepository extends BaseRepository
{

    public function model()
    {
        return Team::class;
    }

    public function getFieldsSearchable(): array
    {
        // TODO: Implement getFieldsSearchable() method.
    }
}
