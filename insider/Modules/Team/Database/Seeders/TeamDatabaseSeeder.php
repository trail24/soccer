<?php

namespace Modules\Team\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Team\Models\Team;

class TeamDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Team::firstOrCreate(['name'=>'Chelsea','strength'=>9]);
        Team::firstOrCreate(['name'=>'Arsenal','strength'=>7]);
        Team::firstOrCreate(['name'=>'Liverpool','strength'=>4]);
        Team::firstOrCreate(['name'=>'Manchester City','strength'=>5]);
    }
}
