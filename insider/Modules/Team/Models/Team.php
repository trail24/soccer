<?php

namespace Modules\Team\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;


class Team extends Authenticatable
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];


}
